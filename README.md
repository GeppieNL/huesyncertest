Installeer dependencies met yarn

Via de Hue App:
1. Maak een entertainment room aan
2. Voeg 1 of meer lampen toe, in je entertainment room

Maak een Hue streaming api key aan door een post te doen naar:
http://<ip-address>/api
```
{
    "devicetype":"myapplication#myiphone",
    "generateclientkey":true
}
```

Zet de volgende gegevens in de dtlshue.js
- ip bridge
- bovengenoemd verkregen api username
- bovengenoemd verkregen api clientkey
- het id van je entertainment room (opvragen met api call, 
    - (gebruik <bridgeip>/debug/clip.html met api call /lights))
- id(s) van je lamp(en) (opvragen met api call)
    - (gebruik <bridgeip>/debug/clip.html met api call /groups kijk naar type entertainment))


Run app met 
```
yarn run dtlshue
```

Error:
```
The DTLS handshake timed out
    at Socket.expectHandshake (xx\src\node_modules\node-dtls-client\build\dtls.js:144:37)
    at Timeout.<anonymous> (xx**\src\node_modules\node-dtls-client\build\dtls.js:102:61)

```

(Leesvoer
https://developers.meethue.com/develop/hue-entertainment/philips-hue-entertainment-api/#getting-started-with-streaming-api )

