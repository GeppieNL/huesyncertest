const http = require('http');
const sockjs = require('sockjs');
const redis = require("redis");
const fs = require('fs');
const express = require('express');

// const expressServer = express.create
// let subscriber = redis.createClient();

let request = require("request");
let dtls = require("node-dtls-client").dtls;

const alpha = 3; // light id Alpha
const bravo = 2; // light id Bravo

const group_id = 3; // entertainment group id

const rgbToXY = require("./rgbToXY");
const hue_hub = "192.168.2.5";
const hue_username = "ZqgGJchUlucli1XGFcJkjpVqcY7G8jy0JKqCKyYj";
const hue_clientkey = Buffer.from("9F850024B183392323DC40545F8FC2DA", "hex");


const options = {
    method: "PUT",
    url: "http://" + hue_hub + "/api/" + hue_username + "/groups/"+group_id,
    json: true,
    body: {
        stream: {
            active: true
        }
    }
}


let echo = sockjs.createServer({ sockjs_url: 'https://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js' });
echo.on('connection', function(conn) {
    conn.on('data', function(message) {
        if(message === 'rood') {
            lights[0].r = 231;
            lights[0].g = 0;
            lights[0].b = 0;
        }

        if(message === 'groen') {
            lights[0].r = 0;
            lights[0].g = 231;
            lights[0].b = 0;
        }

        if(message === 'oranje') {
            lights[0].r = 255;
            lights[0].g = 165;
            lights[0].b = 0;
        }

        if(message === 'start') {
            startStreaming(options);
        }

        console.log('received data', message);
    });
    conn.on('close', function() {
        console.log('closed redis server');
    });
});




let server = http.createServer();
server.addListener('clientjoe', function(req, res) {

    console.log('mathc on httpserver');

    fs.readFile('client.html', function(error, content){
        if(error) {
            console.log(error);
        }

        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(content, 'utf-8');
    });


});

echo.installHandlers(server, { prefix:'/echo' });
server.listen(8080, '0.0.0.0');

let socket = null;

const lights = [
    {
        id: alpha,
        r: 0,
        g: 231,
        b: 0,
        brightness: 53,
        maxbrightness: 50,
        minbrightness: 20,
        sunrise: true,
    },
    {
        id: bravo,
        r: 0,
        g: 231,
        b: 0,
        brightness: 53,
        maxbrightness: 50,
        minbrightness: 20,
        sunrise: true,
    },
];

let nrOfErrors = 0;
let maxErrors = 3;

let running = true;
process.on("SIGINT", () => {
    // Stop example with ctrl+c
    console.log('SIGINT Detected. Shutting down...');
    running = false;
});

function startStreaming(options, callback){

    console.log('starting stream');

    request(options, (err, response, body) => {
        if (response && response.statusCode < 400) {

            console.log('set entertainment room streaming to active', response.body);

            let socketOptions = {
                type: "udp4",
                address: hue_hub,
                port: 2100,
                psk: {},
                timeout: 1000, // in ms, optional, minimum 100, default 1000
                ciphers: [
                    "TLS_PSK_WITH_AES_128_GCM_SHA256",
                    // "TLS_PSK_WITH_AES_256_GCM_SHA384"
                ]
            };
            socketOptions.psk[hue_username] = hue_clientkey;

            //Cipher Suite: TLS_PSK_WITH_AES_256_GCM_SHA384 (0x00a9)

            socket = dtls
                .createSocket(socketOptions, () => {
                    console.log('socket created');
                })
                .on("connected", async() => {

                    console.log('got connected yay');

                    // for(var i=7; i < 1000; i++) {
                    while(running) {

                        // if(i % 2 === 0) {

                        let start = Date.now();

                        let buffer = [
                            Buffer.from("HueStream", "ascii"),
                            // prettier-ignore
                            Buffer.from([
                                0x01, 0x00, //version 1.0
                                0x00, // Sequence ID
                                0x00, 0x00, //reserved
                                0x01, //color space 0x00 = RGB; 0x01 = XY Brightness
                                0x00, // reserved
                            ]),
                        ];

                        lights.forEach(light => {

                            let color = rgbToXY(light.r, light.g, light.b);
                            let lightData = sun(light);

                            buffer.push(Buffer.from([
                                0x00, // Type of device 0x00 = Light
                                0x00, lightData.id, // Unique ID of light
                                color.x.xOne, color.x.xTwo,
                                color.y.yOne, color.y.yTwo,
                                lightData.brightness, lightData.brightness
                            ]));
                        });

                        const message = Buffer.concat(buffer);

                        socket.send(message, e => {

                        });

                        let duration = Date.now() - start;

                        await new Promise(resolve => setTimeout(resolve, 20-duration));
                    }

                    endStreaming();


                })
                .on("message", x => {
                    console.log("received blala", x);
                })
                .on("error", x => {

                    console.log('got an error', x);
                    endStreaming();

                    // startStreaming(options);
                    // throw new Error('got error');

                })
                .on("close", e => {
                    console.log("close", e);
                });
        } else {
            console.log('got an error response', response);
        }

        if(err) {
            console.log('got error when setting entertainmentroom streaming to active: ', err, 'response: ', response);
        }

    });

}


function endStreaming()
{
    request({
        method: "PUT",
        url: "http://" + hue_hub + "/api/" + hue_username + "/groups/"+group_id,
        json: true,
        body: {
            stream: {
                active: false
            }
        }
    }, (err, response, body) => {
        console.log('stream stopped');
    });
}


function sun(light) {

    if(light.sunrise) {
        if(light.brightness < light.maxbrightness) {
            light.brightness++;
        } else {
            light.sunrise = false;
            light.brightness--;
        }
    } else {
        if(light.brightness < light.minbrightness) {
            light.sunrise = true;
            light.brightness++;
        } else {
            light.brightness--;
        }

    }

    return light;

}

try {
    startStreaming(options);

} catch (e) {
    console.log('error, starting for the 2nd time');
    // startStreaming(options);
    // console.log('error, starting for the 3nd time');
    // startStreaming(options);
}

