'use strict';

const express = require('express');
const Phea = require('phea');

// let bridges = Phea.discover();

// 192.168.2.5 is jotihunt

// Constants
// const PORT = 8080;
// const HOST = '0.0.0.0';

// App
// const app = express();
// app.get('/', (req, res) => {
//     res.send('Hello World! Geppiewashere');
//     console.log("bridges: ", bridges);
// });
//
// app.listen(PORT, HOST);
// console.log(`Running on http://${HOST}:${PORT}`);

let running = true;
process.on("SIGINT", () => {
    // Stop example with ctrl+c
    console.log('SIGINT Detected. Shutting down...');
    running = false;
});

// let bridge = Phea.bridge(options);
let groupId = 3;

async function basicExample() {

    let options = {
        "address": "192.168.2.5",
        "username": "LsOap8r1Mb7wLCs5CgVwm0Ybde2VOcQHieupxhvs",
        "psk": "0062FADED63D1828ADEA78FBABF1A0C4",
    }

    let groupId = 3;
    let transitionTime = 1000; // milliseconds

    let bridge = await Phea.bridge(options);
    await bridge.start(groupId);

    while(running) {

        let color = [
            // Generate Random RGB Color Array
            Math.floor(55 + Math.random() * Math.floor(200)),
            Math.floor(55 + Math.random() * Math.floor(200)),
            Math.floor(55 + Math.random() * Math.floor(200))
        ];

        // Set all lights to random color.
        bridge.transition(3, color, transitionTime);

        // Sleep until next color update is needed.
        await new Promise(resolve => setTimeout(resolve, transitionTime));

    }

    bridge.stop();

}

basicExample();


