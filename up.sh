#!/bin/bash
docker stop huesyncer
docker run -p 49160:8080 -d --name=huesyncer geppie/huesyncer -v "$(pwd)/src:/app" sh -c "yarn install && yarn run dev"
#-v huesyncer:/etc/
